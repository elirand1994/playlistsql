import { Request, Response } from "express";
import { ResponseObj } from "../../interfaces/interface.types.js";
import artistServiceSQL from "../../service/SqlService/artist.sql.service.js";
import moderatorServiceSQL from "../../service/SqlService/moderator.sql.service.js";
class ModeratorController {
    updateSongStatus = async (req: Request, res: Response) => {
        req.pipe
        const song = await moderatorServiceSQL.updateSongStatus(req.params.id,req.body);
        const response: ResponseObj = {
            code: 200,
            message: `Changed song ${req.params.id} status to ${req.body.status_id}`,
            data: song,
        };
        res.status(response.code).json(response);
    };

    updateArtistStatus = async (req: Request, res: Response) => {
        const artist = await moderatorServiceSQL.updateArtistStatus(req.params.id,req.body);
        const response: ResponseObj = {
            code: 200,
            message: `Artist with id ${req.params.id} status has changed to ${req.body.status_id}`,
            data: artist,
        };
        res.status(response.code).json(response);
    };

}

const moderatorController = new ModeratorController();
export default moderatorController;
