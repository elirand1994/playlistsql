import HttpException from "../Http.exceptions.js";

class UserFound extends HttpException {
    constructor(email: string) {
        super(404, `This email is already taken! ${email}`);
    }
}

export default UserFound;