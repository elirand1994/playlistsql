import HttpException from "../Http.exceptions.js";

class RoleError extends HttpException {
    constructor(id : string) {
        super(404, `User ${id} doesn't have permitted role!`);
    }
}

export default RoleError;
