import HttpException from "./Http.exceptions.js";
class IdNotValidException extends HttpException {
    constructor(id: string) {
        super(
            403,
            `Id passed in must be a string of 12 bytes or a string of 24 hex characters, passed : ${id}`
        );
    }
}

export default IdNotValidException;
