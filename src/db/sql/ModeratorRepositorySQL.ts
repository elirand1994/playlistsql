import { Artist, Song} from "../../interfaces/interface.types.js";
import { connection as db } from "./sql.connection.js";
import songRepositorySQL from "./SongRepositorySQL.js";
import artistsRepositorySQL from "./ArtistRepositorySQL.js";
class ModeratorRepositorySQL {
    update_song_status = async (id: string, song : Song) => {
        const sql = "UPDATE songs SET ? WHERE song_id = ?";
        await db.query(sql, [song, id]);        
        const song_packet = await songRepositorySQL.get_song_by_id(id);
        return song_packet;
    };
    update_artist_status = async (id: string, artist: Artist) => {
        const sql = "UPDATE artists SET ? WHERE artist_id = ?";
        await db.query(sql, [artist, id]);        
        const artist_packet = await artistsRepositorySQL.get_artist_by_id(id);
        return artist_packet;
    };
}

const moderatorRepositorySQL = new ModeratorRepositorySQL();
export default moderatorRepositorySQL;