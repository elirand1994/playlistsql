import { User } from "../../interfaces/interface.types.js";
import userRepositorySQL from "../../db/sql/UserRepositorySQL.js";
import playlistRepositorySQL from "../../db/sql/PlaylistRepositorySQL.js";
import { delete_all_playlist_song } from "../../utils/helpers.utils.js";
class UserServiceSql {
    createUser = async (body: User) => {
        return await userRepositorySQL.create_user(body);
    };

    getUsertById = async (id: string) => {
        return await userRepositorySQL.get_user_by_id(id);
    };

    getAllUsers = async () => {
        return await userRepositorySQL.get_all_users();
    };

    deleteUserById = async (id: string) => {
        return await userRepositorySQL.delete_user_by_id(id);
    };

    updateUserById = async (id: string, body: User) => {
        return await userRepositorySQL.update_user_by_id(id, body);
    };

    login = async (email: string, password: string) => {
        return await userRepositorySQL.login_user(email, password);
    };

    logout = async (id : string) =>{
        return await userRepositorySQL.logout_user(id);
    }

    deleteAllUsers = async () => {
        await delete_all_playlist_song();
        await playlistRepositorySQL.delete_all_playlists();
        const users = await userRepositorySQL.delete_all_users();
        return users;
    };
}

const userServiceSQL = new UserServiceSql();
export default userServiceSQL;
