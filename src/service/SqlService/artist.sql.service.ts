import { Artist, SongDTO } from "../../interfaces/interface.types.js";
import artistsRepositorySQL from "../../db/sql/ArtistRepositorySQL.js";
import songsRepositorySQL from "../../db/sql/SongRepositorySQL.js";
import { delete_all_playlist_song, delete_song_from_playlist_song } from "../../utils/helpers.utils.js";
class ArtistServiceSQL {
    createArtist = async (body: Artist) => {
        return await artistsRepositorySQL.create_artist(body);
    };

    getArtistById = async (id: string) => {
        return await artistsRepositorySQL.get_artist_by_id(id);
    };

    getAllArtists = async () => {
        return await artistsRepositorySQL.get_all_artists();
    };

    deleteArtistById = async (id: string) => {
        const songs = await songsRepositorySQL.get_all_songs_from_artist_by_id(id) as SongDTO[];
        for (const song of songs) {
            await delete_song_from_playlist_song(song.song_id.toString());
        }

        await songsRepositorySQL.delete_all_songs_by_artist_id(id);
        const deleted_artist_res = await artistsRepositorySQL.delete_artist_by_id(id);

        return deleted_artist_res;
    };

    deleteAllArtists = async () => {
        await delete_all_playlist_song();
        await songsRepositorySQL.delete_all_songs();
        return await artistsRepositorySQL.delete_all_artists();
    };

    updateArtistById = async (id: string, body: Artist) => {
        return await artistsRepositorySQL.update_artist_by_id(id, body);
    };
}

const artistServiceSQL = new ArtistServiceSQL();
export default artistServiceSQL;
