import { Request, Response } from "express";
import { Artist, ResponseObj, Song } from "../../interfaces/interface.types.js";
import moderatorRepositorySQL from "../../db/sql/ModeratorRepositorySQL.js";
import songRepositorySQL from "../../db/sql/SongRepositorySQL.js";
import artistRepositorySQL from "../../db/sql/ArtistRepositorySQL.js";

class ModeratorServiceSQL {
    updateSongStatus = async (id : string, song : Song) => {
        await songRepositorySQL.get_song_by_id(id);
        return await moderatorRepositorySQL.update_song_status(id,song);
    }
    updateArtistStatus = async (id : string, artist : Artist) => {
        await artistRepositorySQL.get_artist_by_id(id);
        return await moderatorRepositorySQL.update_artist_status(id,artist);
    }
}

const moderatorServiceSQL = new ModeratorServiceSQL();
export default moderatorServiceSQL;