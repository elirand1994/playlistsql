import { Song } from "../../interfaces/interface.types.js";
import songsRepositorySQL from "../../db/sql/SongRepositorySQL.js";
import { delete_all_playlist_song } from "../../utils/helpers.utils.js";
class SongServiceSQL {
    createSong = async (body: Song) => {
        return await songsRepositorySQL.create_song(body);
    };

    getSongById = async (id: string) => {
        return await songsRepositorySQL.get_song_by_id(id);
    };

    getAllSongs = async () => {
        return await songsRepositorySQL.get_all_songs();
    };

    deleteAllSongsByArtistId = async (id: string) => {
        return await songsRepositorySQL.delete_all_songs_by_artist_id(id);
    };

    deleteAllSongs = async () => {
        await delete_all_playlist_song();
        return await songsRepositorySQL.delete_all_songs();
    };

    deleteSongById = async (id: string) => {
        return await songsRepositorySQL.delete_song_by_id(id);
    };

    getAllSongsFromArtistById = async (id: string) => {
        return await songsRepositorySQL.get_all_songs_from_artist_by_id(id);
    };

    updateSongById = async (id: string, body: Song) => {
        return await songsRepositorySQL.update_song_by_id(id, body);
    };
}

const songsServiceSQL = new SongServiceSQL();
export default songsServiceSQL;
