import raw from "../../middleware/InitMiddlewares/route.async.wrapper.js";
import express from "express";
import playlistController from "../../controllers/SqlControllers/playlist.controller.js";
import authMiddleware from "../../middleware/AuthMiddlewares/auth.middleware.js";
import { ROLES } from "../../utils/constants.utils.js";
const router = express.Router();
router.use(express.json());

router.get("/", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER,ROLES.MODERATOR])), raw(playlistController.getAllPlaylists)); // DONE
router.get("/:id", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER,ROLES.MODERATOR])), raw(playlistController.getPlaylistById)); // DONE
router.put("/:playlistid/:songid/", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(playlistController.addSongToPlaylist)); // DONE
router.put("/:id/", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(playlistController.updatePlaylistById)); // DONE
router.delete("/:playlistid/:songid", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])),raw(playlistController.deleteSongFromPlaylist)); //Done
router.delete("/:id", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(playlistController.deletePlaylistById)); // DONE
router.delete("/", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER,ROLES.ADMIN])), raw(playlistController.deleteAllPlaylists)); //DONE
router.post("/", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(playlistController.createPlaylist)); // DONE
router.get("/songs/:id",raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])),raw(playlistController.getAllSongsFromPlaylistById));
export default router;
