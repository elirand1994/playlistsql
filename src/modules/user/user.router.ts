import raw from "../../middleware/InitMiddlewares/route.async.wrapper.js";
import express from "express";
import controller from "../../controllers/SqlControllers/user.controller.js";
import { SchemaValidation } from "../../middleware/ValidationMiddlewares/verify_user_scheme.middleware.js";
import authMiddleware from "../../middleware/AuthMiddlewares/auth.middleware.js";
import { ROLES } from "../../utils/constants.utils.js";
const router = express.Router();
router.use(express.json());

router.get("/", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER,ROLES.MODERATOR])),raw(controller.getAllUsers));
router.get("/:id",raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER,ROLES.MODERATOR])), raw(controller.getUserById));
router.put("/:id",raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(SchemaValidation), raw(controller.updateUserById));
router.delete("/:id",raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(controller.deleteUserById));
router.delete("/",raw(authMiddleware.verifyRoles([ROLES.ADMIN])), raw(controller.deleteAllUsers));

export default router;
