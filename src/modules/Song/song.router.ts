import raw from "../../middleware/InitMiddlewares/route.async.wrapper.js";
import express from "express";
import songController from "../../controllers/SqlControllers/song.controller.js";
import authMiddleware from "../../middleware/AuthMiddlewares/auth.middleware.js";
import { ROLES } from "../../utils/constants.utils.js";
const router = express.Router();
router.use(express.json());

router.get("/", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER,ROLES.MODERATOR])), raw(songController.getAllSongs)); //DONE
router.get("/:id", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER,ROLES.MODERATOR])), raw(songController.getSongById)); //DONE
router.put("/:id", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(songController.updateSongById)); //DONE
router.delete("/:id", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(songController.deleteSongById)); //DONE
router.post("/", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(songController.createSong)); //DONE
router.delete("/", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.ADMIN])), raw(songController.deleteAllSongs)); //DONE
router.delete("/artist/:id", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(songController.deleteAllSongsByArtistId)); //DONE
router.get("/artist/:id", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER,ROLES.MODERATOR])), raw(songController.getAllSongsOfArtistById)); //DONE

export default router;
