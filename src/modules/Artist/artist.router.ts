import raw from "../../middleware/InitMiddlewares/route.async.wrapper.js";
import express from "express";
import artistController from "../../controllers/SqlControllers/artist.controller.js";
import authMiddleware from "../../middleware/AuthMiddlewares/auth.middleware.js";
import { ROLES } from "../../utils/constants.utils.js";
const router = express.Router();
router.use(express.json());

router.get("/", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER,ROLES.MODERATOR])), raw(artistController.getAllArtists)); //DONE
router.get("/:id", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER,ROLES.MODERATOR])), raw(artistController.getArtistById)); //DONE
router.put("/:id", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(artistController.updateArtistById)); //DONE
router.delete("/:id", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(artistController.deleteArtistById)); //DONE
router.delete("/", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.ADMIN])), raw(artistController.deleteAllArtists));
router.post("/", raw(authMiddleware.verifyAuth) ,raw(authMiddleware.verifyRoles([ROLES.USER])), raw(artistController.createArtist)); //DONE
export default router;
