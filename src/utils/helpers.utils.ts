import fs from "fs/promises";
import { env } from "../utils/env.js";
import jwt from "jsonwebtoken";
import { connection as db } from "../db/sql/sql.connection.js";
import userRepositorySQL from "../db/sql/UserRepositorySQL.js";
import { exec } from "child_process";
import fetch from 'node-fetch';
import { RowDataPacket } from "mysql2";

export const generateReqId = () => {
    return Math.random().toString(36).slice(2);
};

export const checkIfExists = async (path: string) => {
    try {
        await fs.access(path);
        return true;
    } catch (err) {
        return false;
    }
};

export const createTokens = async (user: any) => {
    const usr = await userRepositorySQL.get_user_by_email(user.email);
    const access_token = jwt.sign(
        { id: usr.user_id, roles : user.roles },
        env.APP_SECRET,
        {
            expiresIn: env.ACCESS_TOKEN_EXPIRATION, // expires in 1 minute
        }
    )

    const refresh_token = jwt.sign(
        { id: usr.user_id },
        env.APP_SECRET,
        {
            expiresIn: env.REFRESH_TOKEN_EXPIRATION, // expires in 60 days... long-term...
        }
    )
    return { access_token, refresh_token };
    };

    //helper functions to delete from playlist_song
    
    export const delete_all_playlist_song = async () =>{
        const sql = "DELETE FROM playlist_song";
        await db.query(sql);
    }

    export const delete_song_from_playlist_song = async (id : string) =>{
        const sql = "DELETE FROM playlist_song where song_id = ?";
        await db.query(sql,id);
    }

    export const get_songs_from_playlist_song = async (id : string) =>{
        const sql = "SELECT * FROM songs AS s JOIN playlist_song AS ps ON s.song_id = ps.song_id WHERE ps.playlist_id = ?";
        const results = await db.query(sql,id);
        const result: RowDataPacket = results[0] as RowDataPacket;
        return result;

    }

    export const backupDB = async () =>{
        const headers = {
            FILENAME : `${Date.now()} - playlist.sql`
        }
        const dump = exec(`docker exec mysql-db /usr/bin/mysqldump -u root --password=qwerty playlist`);
        fetch("http://localhost:8080/backup", {
            method : 'POST',
            headers: headers,
            body : dump.stdout
        });

    }


