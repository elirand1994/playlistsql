export {};
declare global {
    namespace Express {
        interface Request {
            id: string;
            user_id: string;
            user_roles : string;
        }
    }
}
