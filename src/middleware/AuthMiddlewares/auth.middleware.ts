import { NextFunction, Request, RequestHandler, Response } from "express";
import { env } from "../../utils/env.js";
import jwt from "jsonwebtoken";
import { ResponseObj } from "../../interfaces/interface.types.js";
import ValidationException from "../../exceptions/ValidationException.exceptions.js";
import { ROLES } from "../../utils/constants.utils.js";
import RoleError from "../../exceptions/SqlExceptions/RoleError.exceptions.js";
import userRepositorySQL from "../../db/sql/UserRepositorySQL.js";
class AuthMiddleware {
    verifyAuth = async (req: Request, res: Response, next: NextFunction) => {
        try {
            // check header or url parameters or post parameters for token
            const access_token = req.headers["x-access-token"] as string;

            if (!access_token) {
                next(new ValidationException("No token provided."));
            }

            // verifies secret and checks exp
            const decoded = await jwt.verify(access_token, env.APP_SECRET);

            // if everything is good, save to request for use in other routes
            req.user_id = decoded.id;
            req.user_roles = decoded.roles;
            next();
        } catch (error) {
            next(
                new ValidationException(
                    "Failed to authenticate token, access token expired."
                )
            );
        }
    };
    verifyRoles = (permitted_roles : string[]) =>{
        return async (req : Request, res : Response, next : NextFunction) => {
            const user_roles = req.user_roles.split("|");
            if (user_roles.includes(ROLES.ADMIN)) {
                next();
                return;
            }
            console.log(permitted_roles)
            for (const role of user_roles){
                if (permitted_roles.includes(role)){
                    next();
                    return;
                } 
            }
            next(new RoleError(req.user_id));
        }
    };

    getAccessToken = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        const { refresh_token } = req.cookies;
        if (!refresh_token) {
            next(new ValidationException("No refresh token found."));
        }

        try {
            // verifies secret and checks expiration
            const decoded = await jwt.verify(refresh_token, env.APP_SECRET);

            //check user refresh token in DB
            const { id } = decoded;
            const user = await userRepositorySQL.get_user_by_id(id);
            if (user.refresh_token === refresh_token) {
                const access_token = jwt.sign(
                    { id, some: "other value" },
                    env.APP_SECRET,
                    {
                        expiresIn: env.ACCESS_TOKEN_EXPIRATION, //expires in 1 minute
                    }
                );
                const response: ResponseObj = {
                    code: 200,
                    message: "Access token loaded",
                    access_token: access_token,
                };
                res.setHeader("x-access-token", access_token);
                res.status(response.code).json(response);
            } else {
                next(new ValidationException("Access token invalid!"));
            }
        } catch (err) {
            console.log("error: ", err);
            next(new ValidationException("Failed to verify refresh_token."));
        }
    };
}

const authMiddleware = new AuthMiddleware();
export default authMiddleware;
