import raw from "../middleware/InitMiddlewares/route.async.wrapper.js";
import express from "express";
import moderatorController from "../controllers/SqlControllers/moderator.controller.js";
import authMiddleware from "../middleware/AuthMiddlewares/auth.middleware.js";
import { ROLES } from "../utils/constants.utils.js";

const router = express.Router();
router.use(express.json());

router.put("/songstatus/:id", raw(authMiddleware.verifyAuth),raw(authMiddleware.verifyRoles([ROLES.MODERATOR])),moderatorController.updateSongStatus);
router.put("/artiststatus/:id", raw(authMiddleware.verifyAuth),raw(authMiddleware.verifyRoles([ROLES.MODERATOR])),moderatorController.updateArtistStatus);


export default router;
